package testerClasses;

import labUtils.Utils;
import treeClasses.LinkedBinaryTree;

public class MyTester {

	public static void main(String[] args) throws CloneNotSupportedException {

		LinkedBinaryTree<Integer> t = new LinkedBinaryTree<Integer>();
		//LinkedBinaryTree<Integer> t = Utils.buildExampleTreeAsLinkedBinaryTree();

		

		LinkedBinaryTree<Integer> copy;

			copy = t.clone();
			Utils.displayTree("The clone:", copy);
	
	}
}
